package romanNumerals.app;

import romanNumerals.logic.RomanNumeral;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author 000019 on 16/11/2017
 */

public class RomanApp {

    //Scanner object
    private static Scanner keyboard = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Welcome to the roman numeral helper app!\n");

        //Asks the user to choose which type of operations to perform.
        int firstChoice = menu("Select [0-1]:\n" +
                "0 : Conversion operations.\n" +
                "1 : Arithmetic operations.\n", 2);

        switch (firstChoice) {
            case 0:
                //Asks the user which conversion they would like to make
                int conversionChoice = menu("Select [0-1]:\n" +
                        "0 : Convert numeral to decimal value.\n" +
                        "1 : Convert decimal value to numeral.\n", 2);

                //Applies the correct method and deals with invalid inputs in function of the user's conversion choice
                switch (conversionChoice) {
                    case 0:
                        String numeralToConvert = numeralInputControl("Enter numeral to convert");
                        System.out.println(RomanNumeral.convertNumeralToDecValue(numeralToConvert));
                        return;
                    case 1:
                        decConversionInputControl();
                }
                return;
            case 1:
                //Asks the user which operation they would like to perform
                int operationChoice = menu("Select [0-3]:\n" +
                        "0 : Addition.\n" +
                        "1 : Multiplication.\n" +
                        "2 : Subtraction.\n" +
                        "3 : Division.\n", 4);

                //Obtaining user input
                String firstNumeral = numeralInputControl("Enter first numeral");
                String secondNumeral = numeralInputControl("Enter second numeral");
                //Prints the result in function of the user's operation choice
                switch (operationChoice) {
                    case 0:
                        System.out.println(RomanNumeral.addNumerals(firstNumeral, secondNumeral));
                        return;
                    case 1:
                        System.out.println(RomanNumeral.multiplyNumerals(firstNumeral, secondNumeral));
                        return;
                    case 2:
                        System.out.println(RomanNumeral.subtractNumerals(firstNumeral, secondNumeral));
                        return;
                    case 3:
                        System.out.println(RomanNumeral.divideNumerals(firstNumeral, secondNumeral));
                }

        }

        keyboard.close();

    }

    //Generates a menu and exception handling in function of a prompt and a number of options
    private static int menu(String prompt, int numberOfOptions) {
        boolean invalidChoiceInput;
        int choice = 0;
        do {
            invalidChoiceInput = false;
            System.out.println(prompt);
            try {
                choice = keyboard.nextInt();
                keyboard.nextLine();
                if (!(choice >= 0 && choice < numberOfOptions)) {
                    throw new InputMismatchException("Enter a number between 0 and " + (numberOfOptions - 1) + ".");
                }
            } catch (InputMismatchException ime) {
                System.out.println("Please enter a number between 0 and " + (numberOfOptions - 1) + ".");
                invalidChoiceInput = true;
                keyboard.nextLine();
            }
        } while (invalidChoiceInput);
        return choice;
    }

    //Asks for a numeral input and checks validity
    private static String numeralInputControl(String prompt) {
        String numeral;
        do {

            System.out.println(prompt);
            numeral = keyboard.nextLine();
        } while (!RomanNumeral.isValidNumeral(numeral));
        return numeral;
    }

    //Asks for a decimal, checks validity and converts to numeral
    private static void decConversionInputControl() {
        int decimalValue;
        boolean invalidDecimalInput;
        do {
            try {
                invalidDecimalInput = false;
                System.out.println("Enter number");
                decimalValue = keyboard.nextInt();
                keyboard.nextLine();
                System.out.println(RomanNumeral.convertDecValueToNumeral(decimalValue));
            } catch (InputMismatchException ime) {
                System.out.println("Please enter a decimal number");
                invalidDecimalInput = true;
                keyboard.nextLine();
            }
        } while (invalidDecimalInput);
    }

}
