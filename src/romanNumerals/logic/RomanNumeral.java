package romanNumerals.logic;

import java.util.InputMismatchException;

/**
 * This class allows various operations relating to roman numerals.
 *
 * @author 000019 on 16/11/2017
 * @version 1.0
 */

public final class RomanNumeral {

    /**
     * Converts a roman numeral (String) to an equivalent decimal value (int).
     *
     * @param numeral The numeral to be converted.
     * @return The decimal value of the numeral.
     */

    public static int convertNumeralToDecValue(String numeral) throws InputMismatchException {
        int value = 0;
        int[] parsedNumeral = parseNumeral(numeral);
        for (int i = 0; i < parsedNumeral.length - 1; i++) {
            if (parsedNumeral[i] >= parsedNumeral[i + 1]) {
                value += parsedNumeral[i];
            } else {
                value -= parsedNumeral[i];
            }
        }
        value += parsedNumeral[parsedNumeral.length - 1];
        if (!convertDecValueToNumeral(value).equalsIgnoreCase(numeral)) {
            throw new InputMismatchException("Incorrect numeral syntax");
        }
        return value;
    }

    //Method to check if numeral is valid - use this to check inputs
    public static boolean isValidNumeral(String numeral) {
        try {
            convertDecValueToNumeral(convertNumeralToDecValue(numeral)).equals(numeral);
            return true;
        } catch (InputMismatchException ime) {
            System.out.println(ime.getMessage());
            return false;
        }
    }

    //Method to check if any invalid characters are present in input String
    private static boolean containsOnlyNumeralChars(String numeral) {
        String upperCase = numeral.toUpperCase().trim();
        for (int i = 0; i < upperCase.length(); i++) {
            if (!isNumeralChar(upperCase.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    //Method to check if a character is a valid numeral character
    private static boolean isNumeralChar(char character) {
        for (Numeral n : Numeral.values()) {
            if (String.valueOf(n).equals(String.valueOf(character))) {
                return true;
            }
        }
        return false;
    }

    /* Method which converts the numeral String into an array of int with the
     * corresponding values to the numeral symbols in order */
    private static int[] parseNumeral(String numeral) throws InputMismatchException {
        if (!containsOnlyNumeralChars(numeral)) {
            throw new InputMismatchException("Contains incorrect characters");
        }
        char[] charArray = numeral.toUpperCase().toCharArray();
        int[] intArray = new int[charArray.length];
        for (int i = 0; i < charArray.length; i++) {
            for (Numeral n : Numeral.values()) {
                if (n.name().equals(String.valueOf(charArray[i]))) {
                    intArray[i] = n.value;
                }
            }
        }
        return intArray;
    }

    /**
     * Converts a decimal value (int) to a roman numeral (String).
     *
     * @param decimalValue The decimal value to be converted.
     * @return The roman numeral equivalent of the decimal value.
     */

    public static String convertDecValueToNumeral(int decimalValue) {
        int input = decimalValue < 0 ? -decimalValue : decimalValue;
        return (decimalValue < 0 ? "-" : "") + getThousands(input) + getHundreds(input) + getTens(input) + getUnits(input);
    }

    private static String getThousands(int decimalValue) {
        return repeatNumerals(decimalValue, Numeral.M.getValue(), Numeral.M.name());
    }

    private static String getHundreds(int decimalValue) {
        return numeralLogic(setScope(decimalValue, 1000), Numeral.M, Numeral.D, Numeral.C);
    }

    private static String getTens(int decimalValue) {
        return numeralLogic(setScope(decimalValue, 100), Numeral.C, Numeral.L, Numeral.X);
    }

    private static String getUnits(int decimalValue) {
        return numeralLogic(setScope(decimalValue, 10), Numeral.X, Numeral.V, Numeral.I);
    }

    //Method generalizing the logic pattern for numeral inversion
    private static String numeralLogic(int decimalValue, Numeral multTen, Numeral multFive, Numeral unit) {
        if (isLowerThanFour(decimalValue, unit)) {
            return lowerThanFourSyntax(decimalValue, unit);
        } else if (isLowerThanFive(decimalValue, unit)) {
            return fourSyntax(unit, multFive);
        } else if (isLowerThanNine(decimalValue, unit)) {
            return greaterThanFiveSyntax(decimalValue, unit, multFive);
        } else {
            return nineSyntax(unit, multTen);
        }
    }

    //Method returns syntax for nine
    private static String nineSyntax(Numeral unit, Numeral multTen) {
        return unit.name() + multTen.name();
    }

    //Method returns syntax for numbers between five and nine
    private static String greaterThanFiveSyntax(int decimalValue, Numeral unit, Numeral multFive) {
        return multFive.name() +
                repeatNumerals(decimalValue - multFive.getValue(), unit.getValue(), unit.name());
    }

    //Method returns syntax for four
    private static String fourSyntax(Numeral unit, Numeral multFive) {
        return unit.name() + multFive.name();
    }

    //Method returns the syntax of numbers below four
    private static String lowerThanFourSyntax(int decimalValue, Numeral unit) {
        return repeatNumerals(decimalValue, unit.getValue(), unit.name());
    }

    //Method to check if the has a multiple of nine syntax
    private static boolean isLowerThanNine(int decimalValue, Numeral unit) {
        if (decimalValue / (9 * unit.getValue()) < 1) {
            return true;
        } else {
            return false;
        }
    }

    //Method to check if the has a multiple of five syntax
    private static boolean isLowerThanFive(int decimalValue, Numeral unit) {
        if (decimalValue / (5 * unit.getValue()) < 1) {
            return true;
        } else {
            return false;
        }
    }

    //Method to check if the has a multiple of four syntax
    private static boolean isLowerThanFour(int decimalValue, Numeral unit) {
        if (decimalValue / (4 * unit.getValue()) < 1) {
            return true;
        } else {
            return false;
        }
    }

    //Method to produce a String of the same repeating numeral
    private static String repeatNumerals(int decimalValue, int unit, String numeralCharacter) {
        int temp = decimalValue / unit;
        StringBuilder builder = new StringBuilder("");
        while (temp > 0) {
            builder.append(numeralCharacter);
            temp--;
        }
        return builder.toString();
    }

    //Method to reduce the scope of the numeralLogic method to the correct order of magnitude
    private static int setScope(int value, int multipleOfTen) {
        while (value / multipleOfTen > 0) {
            value -= multipleOfTen;
        }
        return value;
    }

    /**
     * This method adds numerals and returns the result as a numeral String
     *
     * @param firstNumeral
     * @param secondNumeral
     * @return
     */
    public static String addNumerals(String firstNumeral, String secondNumeral) {
        return convertDecValueToNumeral(convertNumeralToDecValue(firstNumeral)
                + convertNumeralToDecValue(secondNumeral));
    }

    /**
     * This method multiplies numerals and returns the result as a numeral String
     *
     * @param firstNumeral
     * @param secondNumeral
     * @return
     */
    public static String multiplyNumerals(String firstNumeral, String secondNumeral) {
        return convertDecValueToNumeral(convertNumeralToDecValue(firstNumeral)
                * convertNumeralToDecValue(secondNumeral));
    }

    /**
     * This method subtracts numerals and returns the result as a numeral String
     *
     * @param firstNumeral
     * @param secondNumeral
     * @return
     */
    public static String subtractNumerals(String firstNumeral, String secondNumeral) {
        return convertDecValueToNumeral(convertNumeralToDecValue(firstNumeral)
                - convertNumeralToDecValue(secondNumeral));
    }

    /**
     * This method divides numerals and returns the result as a numeral String
     *
     * @param firstNumeral
     * @param secondNumeral
     * @return
     */
    public static String divideNumerals(String firstNumeral, String secondNumeral) {
        return convertDecValueToNumeral(convertNumeralToDecValue(firstNumeral)
                / convertNumeralToDecValue(secondNumeral));
    }


    /**
     * This enum contains the 7 characters used in roman numerals and their respective values.
     */

    public enum Numeral {
        I(1), V(5), X(10), L(50), C(100), D(500), M(1000);

        private int value;

        Numeral(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

}
